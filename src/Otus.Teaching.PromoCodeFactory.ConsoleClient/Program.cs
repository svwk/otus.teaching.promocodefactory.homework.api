﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.GrpcService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.ConsoleClient.Models;

namespace Otus.Teaching.PromoCodeFactory.ConsoleClient
{
    class Program
    {
        static HubConnection _hubConnection;

        static async Task Main()
        {            
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Choose client:\n g - grpc,\n s - SignalR, \n x - Exit");
                char choice = Console.ReadKey().KeyChar;

                if (choice == 'g') await GrpcClient();
                else if (choice == 's') await SignalRClient();
                else if (choice == 'x') break;
            }
           

            
        }

        private static async Task GrpcClient()
        {
            var httpHandler = new HttpClientHandler();
            // Return `true` to allow certificates that are untrusted/invalid
            httpHandler.ServerCertificateCustomValidationCallback = 
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;

            var channel = GrpcChannel.ForAddress("https://localhost:5001",
                new GrpcChannelOptions { HttpHandler = httpHandler });
            
            var client = new CustomerGrpcService.CustomerGrpcServiceClient(channel);
            
            List<string> idList;
            idList = await grpcoperations.ShowAll(client);
            _ = Console.ReadKey();

            while (idList?.Count > 0)
            {
                // create
                await grpcoperations.Create(client);
                _ = Console.ReadKey();

                idList = await grpcoperations.ShowAll(client);
                _ = Console.ReadKey();


                string id = idList[idList.Count - 1];
                //get by id
                await grpcoperations.GetById(client, id);

                //edit
                await grpcoperations.Edit(client, id);
                await grpcoperations.GetById(client, id);
                _ = Console.ReadKey();
                await grpcoperations.ShowAll(client);
                _ = Console.ReadKey();


                //delete
                await grpcoperations.Delete(client, idList[idList.Count - 2]);

                idList = await grpcoperations.ShowAll(client);
                _ = Console.ReadKey();

                Console.WriteLine("Continue? n- no");
                if (Console.ReadKey().KeyChar == 'n') break;
            }
           
        }

        private static async Task SignalRClient()
        {                       
            await InitSignalRConnection();
                bool isExit = false;
                while (!isExit)
                {
                    Console.WriteLine("Available commands:");
                    Console.WriteLine("x or exit: for exit");
                    Console.WriteLine("gc: for GetCustomer");
                    Console.WriteLine("gcs: for GetCustomers");
                    Console.WriteLine("cc: for CreateCustomer");
                    Console.WriteLine("ec: for EditCustomer");
                    Console.WriteLine("dc: for DeleteCustomer");
                    Console.WriteLine("Enter your message or command:");

                    var userInput = Console.ReadLine();

                    //if (string.IsNullOrWhiteSpace(userInput))
                    //    continue;

                    switch (userInput)
                    {
                        case "exit":
                        case "x":
                            isExit = true;
                            break;
                        case "gc":
                            {
                                Console.WriteLine("Enter guid of customer:");
                                var strGuid = Console.ReadLine();
                                if (string.IsNullOrWhiteSpace(strGuid))
                                    break;
                                if (Guid.TryParse(strGuid, out var valueGuid))
                                {
                                    await _hubConnection.SendAsync("GetCustomer", valueGuid);
                                    Console.WriteLine($"Message 'GetCustomer'({valueGuid}) sent");
                                }
                            }
                            break;
                        case "gcs":
                            await _hubConnection.SendAsync("GetCustomers");
                            Console.WriteLine("Message 'GetCustomers' sent");
                            break;
                        case "cc":
                            {
                                Console.WriteLine("Enter FirstName of customer:");
                                var firstName = Console.ReadLine();
                                if (string.IsNullOrWhiteSpace(firstName)) break;
                                Console.WriteLine("Enter LastName of customer:");
                                var lastName = Console.ReadLine();
                                if (string.IsNullOrWhiteSpace(lastName)) break;
                                Console.WriteLine("Enter Email of customer:");
                                var email = Console.ReadLine();
                                if (string.IsNullOrWhiteSpace(email)) break;
                                Console.WriteLine("Enter PreferenceIds of customer (list with ','):");
                                var ids = Console.ReadLine();
                                List<Guid> GuidList = new List<Guid>();
                                if (!string.IsNullOrWhiteSpace(ids))
                                {
                                    List<string> IdList = ids.Split(',').ToList();
                                    Guid Id = Guid.Empty;
                                    GuidList.AddRange(from strGuid in IdList where Guid.TryParse(strGuid, out Id) select Id);
                                }
                                CreateOrEditCustomerRequest customer = new CreateOrEditCustomerRequest()
                                {
                                    FirstName = firstName,
                                    LastName = lastName,
                                    Email = email,
                                    PreferenceIds = GuidList
                                };

                                var result = await _hubConnection.InvokeAsync<string>("CreateCustomer", customer);
                                Console.WriteLine($"Message 'CreateCustomer'({firstName}) sent");
                                Console.WriteLine($"Result of operation is: {result}");
                            }
                            break;
                        case "ec":
                            {
                                Console.WriteLine("Enter guid of customer:");
                                var IdGuid = Console.ReadLine();
                                if (string.IsNullOrWhiteSpace(IdGuid))
                                    break;
                                if (!Guid.TryParse(IdGuid, out var valueGuid))
                                    break;

                                Console.WriteLine("Enter FirstName of customer:");
                                var firstName = Console.ReadLine();
                                if (string.IsNullOrWhiteSpace(firstName)) break;
                                Console.WriteLine("Enter LastName of customer:");
                                var lastName = Console.ReadLine();
                                if (string.IsNullOrWhiteSpace(lastName)) break;
                                Console.WriteLine("Enter Email of customer:");
                                var email = Console.ReadLine();
                                if (string.IsNullOrWhiteSpace(email)) break;

                                Console.WriteLine("Enter PreferenceIds of customer (list with ','):");
                                var ids = Console.ReadLine();
                                List<Guid> GuidList = new List<Guid>();
                                if (!string.IsNullOrWhiteSpace(ids))
                                {
                                    List<string> IdList = ids.Split(',').ToList();
                                    Guid Id = Guid.Empty;
                                    GuidList.AddRange(from strGuid in IdList where Guid.TryParse(strGuid, out Id) select Id);
                                }

                                CreateOrEditCustomerRequest customerc = new CreateOrEditCustomerRequest()
                                {
                                    FirstName = firstName,
                                    LastName = lastName,
                                    Email = email,
                                    PreferenceIds = GuidList
                                };

                                await _hubConnection.SendAsync("EditCustomer", valueGuid, customerc);
                                Console.WriteLine($"Message 'EditCustomer'({firstName}) sent");
                            }
                            break;
                        case "dc":
                            {
                                Console.WriteLine("Enter guid of customer:");
                                var strGuid = Console.ReadLine();
                                if (string.IsNullOrWhiteSpace(strGuid))
                                    break;
                                if (Guid.TryParse(strGuid, out var valueGuid))
                                {
                                    await _hubConnection.SendAsync("DeleteCustomer", valueGuid);
                                    Console.WriteLine($"Message 'DeleteCustomer'({valueGuid}) sent");
                                }
                            }
                            break;
                    }
                }
                Console.ReadLine();

                //await _hubConnection.InvokeAsync<string>("SubscribeOnStock", "MSFT");


                //Console.ReadLine();
                await _hubConnection.StopAsync();
            }

            private static Task InitSignalRConnection()
            {
                _hubConnection = new HubConnectionBuilder()
                    .WithUrl("http://localhost:5010/hubs/customers")
                    .WithAutomaticReconnect()
                    .AddMessagePackProtocol()
                    .Build();

                _hubConnection.On("ShowCustomers", (List<CustomerShortResponse> customerList) =>
                {
                    Console.WriteLine(new string('-', 40));
                    Console.WriteLine($"Received customers:");
                    Console.WriteLine(new string('-', 40));
                    foreach (var customer in customerList)
                    {
                        Console.WriteLine($"Id: {customer.Id}");
                        Console.WriteLine($"FirstName: {customer.FirstName}");
                        Console.WriteLine($"LastName: {customer.LastName}");
                        Console.WriteLine($"Email: {customer.Email}");
                        Console.WriteLine(new string('-', 30));
                    }
                    Console.WriteLine(new string('-', 40));
                });

                _hubConnection.On("ShowCustomer", (CustomerResponse customer) =>
                {
                    Console.WriteLine(new string('-', 40));
                    Console.WriteLine($"Received customer:");
                    Console.WriteLine(new string('-', 40));
                    Console.WriteLine($"Id: {customer.Id}");
                    Console.WriteLine($"FirstName: {customer.FirstName}");
                    Console.WriteLine($"LastName: {customer.LastName}");
                    Console.WriteLine($"Email: {customer.Email}");
                    Console.WriteLine($"Preferences: ");
                    foreach (var preference in customer.Preferences)
                    {
                        Console.WriteLine($"{preference.Id}: {preference.Name}");
                    }
                    Console.WriteLine(new string('-', 40));
                }
                );

                _hubConnection.On("ShowMessage", (string messageText) =>
                {
                    Console.WriteLine(new string('-', 40));
                    Console.WriteLine($"Received message:");
                    Console.WriteLine($"{messageText}");
                    Console.WriteLine(new string('-', 40));
                }
                );

                return _hubConnection.StartAsync();
            }
        
 
    }
}

﻿using Google.Protobuf.WellKnownTypes;
using Otus.Teaching.PromoCodeFactory.GrpcService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.ConsoleClient
{
    static class grpcoperations
    {
        public static async Task<List<string>> ShowAll(CustomerGrpcService.CustomerGrpcServiceClient client)
        {
            List<string> list = new List<string>();
            Console.WriteLine("All");
            var customerList = await client.GetAllCustomersAsync(new Empty());
            Console.WriteLine(new string('-', 40));
            if (customerList.Status.Status != 0)
            {
                Console.WriteLine($"ERROR {customerList.Status.ErrorName}");
                Console.WriteLine($"{customerList.Status.Text}");
                Console.WriteLine(new string('-', 40));
            }
            else
            {
                Console.WriteLine($"Received customers:");
                Console.WriteLine(new string('-', 40));
                foreach (var customer in customerList.CustomerList)
                {
                    Console.WriteLine($"Id: {customer.Id}");
                    Console.WriteLine($"FirstName: {customer.FirstName}");
                    Console.WriteLine($"LastName: {customer.LastName}");
                    Console.WriteLine($"Email: {customer.Email}");
                    Console.WriteLine(new string('-', 30));
                    list.Add(customer.Id);
                }

                Console.WriteLine(new string('-', 40));
            }

            return (list);
        }

        public static async Task Create(CustomerGrpcService.CustomerGrpcServiceClient client)
        {
            Console.WriteLine("create");
            Console.WriteLine(new string('-', 40));
            var createResult1 = await client.CreateCustomerAsync(new GrpcEditCustomerRequest()
            {
                Email = "fff@fff.ff",
                FirstName = "aaa",
                LastName = "bbb",
                PreferenceIds = {"ef7f299f-92d7-459f-896e-078ed53ef99c"}
            });
            if (createResult1.Status.Status != 0)
            {
                Console.WriteLine($"ERROR {createResult1.Status.ErrorName}");
            }

            Console.WriteLine($"Result: {createResult1.Status.Text}");
            Console.WriteLine(new string('-', 30));

            var createResult2 = await client.CreateCustomerAsync(new GrpcEditCustomerRequest()
                {
                    Email = "ggg@ggg.gg",
                    FirstName = "uuu",
                    LastName = "iii",
                    PreferenceIds = {"c4bda62e-fc74-4256-a956-4760b3858cbd"}
                }
            );
            if (createResult2.Status.Status != 0)
            {
                Console.WriteLine($"ERROR {createResult2.Status.ErrorName}");
            }

            Console.WriteLine($"Result: {createResult2.Status.Text}");
            Console.WriteLine(new string('-', 40));
        }

        public static async Task GetById(CustomerGrpcService.CustomerGrpcServiceClient client, string id)
        {
            Console.WriteLine("get by id");
            var findResult = await client.GetCustomerByIdAsync(new GrpcStringGuid()
            {
                Id = id // "ef7f299f-92d7-459f-896e-078ed53ef99c" //createResult1.Id                
            });
            Console.WriteLine(new string('-', 40));
            if (findResult.Status.Status != 0)
            {
                Console.WriteLine($"ERROR {findResult.Status.ErrorName}");
                Console.WriteLine($"{findResult.Status.Text}");
            }
            else
            {
                Console.WriteLine($"Received customer:");
                Console.WriteLine(new string('-', 40));
                Console.WriteLine($"Id: {findResult.Id}");
                Console.WriteLine($"FirstName: {findResult.FirstName}");
                Console.WriteLine($"LastName: {findResult.LastName}");
                Console.WriteLine($"Email: {findResult.Email}");
                Console.WriteLine($"Preferences: ");
                foreach (var preference in findResult.Preferences)
                {
                    Console.WriteLine($"{preference.Id}: {preference.Name}");
                }
            }

            Console.WriteLine(new string('-', 40));
        }

        public static async Task Edit(CustomerGrpcService.CustomerGrpcServiceClient client, string id)
        {
            Console.WriteLine("edit");
            var editResult = await client.EditCustomerAsync(new GrpcEditCustomerRequest()
            {
                Id = id,
                FirstName = "kkk",
                LastName = "hhh",
                Email = "rrr@rrr.rr",
                PreferenceIds = {"76324c47-68d2-472d-abb8-33cfa8cc0c84"}
            });
            if (editResult.Status.Status != 0)
            {
                Console.WriteLine($"ERROR {editResult.Status.ErrorName}");
            }

            Console.WriteLine($"Result: {editResult.Status.Text}");
            Console.WriteLine(new string('-', 40));
        }

        public static async Task Delete(CustomerGrpcService.CustomerGrpcServiceClient client, string id)
        {
            Console.WriteLine("delete");
            var deleteResult = await client.DeleteCustomerAsync(new GrpcStringGuid()
            {
                Id = id
            });
            if (deleteResult.Status.Status != 0)
            {
                Console.WriteLine($"ERROR {deleteResult.Status.ErrorName}");
            }

            Console.WriteLine($"Result: {deleteResult.Status.Text}");
            Console.WriteLine(new string('-', 40));
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Models
{
    public class CustomerMutation
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerMutation(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<Customer> Create(CreateOrEditCustomerRequest request)
        {
            IEnumerable<Preference> preferences = new List<Preference>();
            //Получаем предпочтения из бд и сохраняем большой объект
            if (request.PreferenceIds != null && request.PreferenceIds.Any())
            {
                preferences = await _preferenceRepository.GetRangeByIdsAsync(
                        request.PreferenceIds);
            }

            var customer = CustomerMapper.MapFromModel(request, preferences);
            await _customerRepository.AddAsync(customer);

            return customer;
        }

        public async Task<Customer> Edit(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return null;

            IEnumerable<Preference> preferences = new List<Preference>();
            //Получаем предпочтения из бд и сохраняем большой объект
            if (request.PreferenceIds != null && request.PreferenceIds.Any())
            {
                preferences = await _preferenceRepository.GetRangeByIdsAsync(
                    request.PreferenceIds);
            }
            

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return customer;
        }

        public async Task<Customer> Delete(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return null;

            await _customerRepository.DeleteAsync(customer);

            return customer;
        }

    }
}
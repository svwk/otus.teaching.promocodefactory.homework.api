using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GrpcService.Mappers;

namespace Otus.Teaching.PromoCodeFactory.GrpcService
{
    public class GrpcCustomerService : CustomerGrpcService.CustomerGrpcServiceBase
    {
        private readonly ILogger<GrpcCustomerService> _logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public GrpcCustomerService(ILogger<GrpcCustomerService> logger,
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override Task<GrpcShortCustomerList> GetAllCustomers(Empty request, ServerCallContext context)
        {
            var customers = _customerRepository.GetAllAsync().Result;
            //var customers =await  _customerRepository.GetAllAsync();
            GrpcShortCustomerList list = new GrpcShortCustomerList();
            list.Status = new GrpcResponceStatus {Status = 1};
            if (customers != null && customers.Any())
            {
                list.CustomerList.AddRange(
                    customers.Select(x => CustomerMapper.ToGrpcShortCustomer(x)).ToList());
                list.Status.Status = 0;
            }

            return Task.FromResult(list);
            //return list;            
        }


        public override Task<GrpcCustomerResponse> GetCustomerById(GrpcStringGuid request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
                //throw new RpcException(new Status(StatusCode.InvalidArgument, "Id is wrong"));
                return Task.FromResult(new GrpcCustomerResponse
                {
                    Status = new GrpcResponceStatus
                    {
                        Status = 1,
                        ErrorName = "InvalidArgument", Text = "Id is wrong"
                    }
                });

            var customer = _customerRepository.GetByIdAsync(id).Result;
            if (customer == null)
                //throw new RpcException(new Status(StatusCode.NotFound, $"Customer with Id {id} isn't found"));               
                return Task.FromResult(new GrpcCustomerResponse
                {
                    Status = new GrpcResponceStatus
                    {
                        Status = 1,
                        ErrorName = "NotFound",
                        Text = $"Customer with Id {id} isn't found"
                    }
                });

            return Task.FromResult(CustomerMapper.ToGrpcCustomerResponse(customer));
        }

        public override Task<GrpcStringGuid> CreateCustomer(GrpcEditCustomerRequest request, ServerCallContext context)
        {
            IEnumerable<Preference> preferences = new List<Preference>();
            //Получаем предпочтения из бд и сохраняем большой объект
            if (request.PreferenceIds != null && request.PreferenceIds.Any())
            {
                preferences = _preferenceRepository.GetRangeByIdsAsync(
                        request.PreferenceIds.Select(x => Guid.Parse(x)).ToList())
                    .Result;
            }


            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            try
            {
                _customerRepository.AddAsync(customer);
            }
            catch (Exception e)
            {
                //throw new RpcException(new Status(StatusCode.Aborted, e.Message));                
                return Task.FromResult(new GrpcStringGuid
                {
                    Status = new GrpcResponceStatus
                    {
                        Status = 1,
                        ErrorName = "Aborted",
                        Text = $"Customer {request.FirstName} {request.LastName} was not created. Error: {e.Message}"
                    }
                });
            }

            return Task.FromResult(new GrpcStringGuid
            {
                Id = customer.Id.ToString(), Status = new GrpcResponceStatus
                {
                    Status = 0,
                    Text = $"Customer with Id={customer.Id.ToString()} is created  succesfully."
                }
            });
        }


        public override Task<GrpcStringGuid> EditCustomer(GrpcEditCustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
                //throw new RpcException(new Status(StatusCode.InvalidArgument, "Id is wrong"));
                return Task.FromResult(new GrpcStringGuid
                {
                    Status = new GrpcResponceStatus
                    {
                        Status = 1,
                        ErrorName = "InvalidArgument",
                        Text = "Id is wrong"
                    }
                });

            var customer = _customerRepository.GetByIdAsync(id).Result;

            if (customer == null)
                //throw new RpcException(new Status(StatusCode.NotFound, $"Customer with Id {id} isn't found"));
                return Task.FromResult(new GrpcStringGuid
                {
                    Status = new GrpcResponceStatus
                    {
                        Status = 1,
                        ErrorName = "NotFound",
                        Text = $"Customer with Id {id} wasn't found"
                    }
                });

            IEnumerable<Preference> preferences = new List<Preference>();
            //Получаем предпочтения из бд и сохраняем большой объект
            if (request.PreferenceIds != null && request.PreferenceIds.Any())
            {
                preferences = _preferenceRepository.GetRangeByIdsAsync(
                    request.PreferenceIds.Select(x => Guid.Parse(x)).ToList()).Result;
            }


            CustomerMapper.MapFromModel(request, preferences, customer);
            try
            {
                _customerRepository.UpdateAsync(customer);
            }
            catch (Exception e)
            {
                //throw new RpcException(new Status(StatusCode.Aborted, e.Message));

                return Task.FromResult(new GrpcStringGuid
                {
                    Status = new GrpcResponceStatus
                    {
                        Status = 1,
                        ErrorName = "Aborted",
                        Text = $"Customer with Id {id.ToString()} was not edited. Error: {e.Message}"
                    }
                });
            }

            return Task.FromResult(new GrpcStringGuid
            {
                Id = id.ToString(),
                Status = new GrpcResponceStatus
                {
                    Status = 0,
                    Text = $"Customer with Id={customer.Id.ToString()} is created  succesfully."
                }
            });
        }


        public override Task<GrpcStringGuid> DeleteCustomer(GrpcStringGuid request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
                //throw new RpcException(new Status(StatusCode.InvalidArgument, "Id is wrong"));
                return Task.FromResult(new GrpcStringGuid
                {
                    Status = new GrpcResponceStatus
                    {
                        Status = 1,
                        ErrorName = "InvalidArgument",
                        Text = "Id is wrong"
                    }
                });

            var customer = _customerRepository.GetByIdAsync(id).Result;

            if (customer == null)
                //throw new RpcException(new Status(StatusCode.NotFound, $"Customer with Id {id} isn't found"));
                return Task.FromResult(new GrpcStringGuid
                {
                    Status = new GrpcResponceStatus
                    {
                        Status = 1,
                        ErrorName = "NotFound",
                        Text = $"Customer with Id {id} wasn't found"
                    }
                });


            try
            {
                _customerRepository.DeleteAsync(customer);
            }
            catch (Exception e)
            {
                return Task.FromResult(new GrpcStringGuid
                {
                    Status = new GrpcResponceStatus
                    {
                        Status = 1,
                        ErrorName = "Aborted",
                        Text = $"Customer with Id {id.ToString()} was not deleted. Error: {e.Message}"
                    }
                });
            }

            //return $"Customer with Id={customer.Id.ToString()} is deleted succesfully.";           
            return Task.FromResult(new GrpcStringGuid
            {
                Id = id.ToString(),
                Status = new GrpcResponceStatus
                {
                    Status = 0,
                    Text = $"Customer with Id={customer.Id.ToString()} was deleted  succesfully."
                }
            });
        }
    }
}
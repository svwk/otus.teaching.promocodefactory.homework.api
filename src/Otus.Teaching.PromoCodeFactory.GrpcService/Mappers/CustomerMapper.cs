﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GrpcService.Mappers
{
    public static class CustomerMapper
    {
        public static GrpcShortCustomer ToGrpcShortCustomer(Customer customer)
        {
            if (customer == null) return null;

            return new GrpcShortCustomer()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };            
        }

        public static GrpcCustomerResponse ToGrpcCustomerResponse(Customer customer)
        {
            if (customer == null) return null;

            var result = new GrpcCustomerResponse()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Status = new GrpcResponceStatus { Status = 0}
            };
            result.Preferences.AddRange(
                   customer.Preferences.Select(x =>
                           new GrpcPreferenceResponse
                           {
                               Id = x.PreferenceId.ToString(),
                               Name = x.Preference.Name,
                           }
                       ).ToList()
                       );
            return result;
        }

        public static Customer MapFromModel(GrpcEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if (customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }

            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            if (customer.Preferences != null) customer?.Preferences.Clear() ;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,                
                PreferenceId = x.Id,
                Customer=customer,
                Preference=x
            }).ToList();

            return customer;
        }


    }
}

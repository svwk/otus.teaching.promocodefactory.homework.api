using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer.Hubs
{
    public interface ICustomersHubClient
    {
        Task ShowCustomers(List<CustomerShortResponse> customerList);
        Task ShowCustomer(CustomerResponse customer);
        Task ShowMessage(string messageText);
    }
}
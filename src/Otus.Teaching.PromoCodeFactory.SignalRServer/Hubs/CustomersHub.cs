using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer.Hubs
{
    public class CustomersHub: Hub<ICustomersHubClient>
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        
        public CustomersHub(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        
        public async Task GetCustomers()
        {
            var customers =  await _customerRepository.GetAllAsync();
            if (customers == null)
            {
                await Clients.Caller.ShowMessage($"There are no customers in DB");
            }
            else
            {
                var response = customers.Select(x => new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList();

                //return response;
                await Clients.Caller.ShowCustomers(response);
            }
        }

        public async Task GetCustomer(Guid id)
        {
            var customer =  await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                await Clients.Caller.ShowMessage($"Customer with Id {id} isn't found");
            }
            else
            {
                var response = new CustomerResponse(customer);
                
                            //return response;
                await Clients.Caller.ShowCustomer(response);
            }
            
        }

        public async Task<string> CreateCustomer(CreateOrEditCustomerRequest request)
        {
            IEnumerable<Preference> preferences = new List<Preference>();
            //Получаем предпочтения из бд и сохраняем большой объект
            if (request.PreferenceIds != null && request.PreferenceIds.Any())
            {
                 preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            }
           

            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            try
            {
                await _customerRepository.AddAsync(customer);
            }
            catch (Exception e)
            {
                return $"Customer {request.FirstName} {request.LastName} was not created. Error: {e.Message}";
            }

            var response = new CustomerResponse(customer);
            await Clients.Caller.ShowCustomer(response);

            return $"Customer with Id={customer.Id.ToString()} is created  succesfully.";
        }

        public async Task<string> EditCustomer(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
            {
                return ($"Customer with Id {id} isn't found");
            }
            
            IEnumerable<Preference> preferences = new List<Preference>();
            //Получаем предпочтения из бд и сохраняем большой объект
            if (request.PreferenceIds != null && request.PreferenceIds.Any())
            {
                preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            }
            
            CustomerMapper.MapFromModel(request, preferences, customer);

            try
            {
                await _customerRepository.UpdateAsync(customer);
            }
            catch (Exception e)
            {
                return $"Customer {request.FirstName} {request.LastName} was not edited. Error: {e.Message}";
            }

            var response = new CustomerResponse(customer);
            await Clients.Caller.ShowCustomer(response);

            return $"Customer with Id={customer.Id.ToString()} is edited succesfully.";
        }

        public async Task<string> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
            {
                return ($"Customer with Id {id.ToString()} isn't found");
            }
            try
            {
                await _customerRepository.DeleteAsync(customer);
            }
            catch (Exception e)
            {
                return $"Customer with Id {id.ToString()} was not deleted. Error: {e.Message}";
            }

            return $"Customer with Id={customer.Id.ToString()} is deleted succesfully.";
        }
        
    }
}